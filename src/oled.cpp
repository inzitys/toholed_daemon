#include <QImage>
#include <QPainter>

#include <linux/i2c-dev.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <time.h>
#include "math.h"
#include "oled.h"
#include "charger.h"
#include "jollafontti.h"
#include "pienifontti.h"
#include "icons.h"
#include "derp.h"
#include "updateTime.h"

#define DBGPRINT

analogHand minuteHand;
analogHand hourHand;

/* Draws derp logo, Fullscreen bmp */
void drawDerp(char *screenBuffer)
{
    QImage sb((unsigned char *)screenBuffer, 64, 128, QImage::Format_MonoLSB);
    QImage derp((const unsigned char *)derpBitmaps, 128, 64, QImage::Format_Mono);

    sb.setColorTable(QVector<QRgb>() << qRgb(0, 0, 0) << qRgb(255, 255, 255));
    derp.setColorTable(QVector<QRgb>() << qRgb(0, 0, 0) << qRgb(255, 255, 255));

    QPainter p;
    p.begin(&sb);
    p.rotate(90);
    p.scale(1, -1);
    p.drawImage(0, 0, derp, 0, 0, derp.width(), derp.height());
    p.end();
}

/* Draws updateTime image, Fullscreen bmp */
void drawUpdateTime(char *screenBuffer)
{
    QImage sb = QImage((unsigned char *)screenBuffer, 64, 128, QImage::Format_MonoLSB);
    QImage updateTime((unsigned char *)updateTimeBitmaps, 128, 64, QImage::Format_Mono);

    sb.setColorTable(QVector<QRgb>() << qRgb(0, 0, 0) << qRgb(255, 255, 255));
    updateTime.setColorTable(QVector<QRgb>() << qRgb(0, 0, 0) << qRgb(255, 255, 255));

    QPainter p;
    p.begin(&sb);
    p.rotate(90);
    p.scale(1, -1);
    p.drawImage(0, 0, updateTime, 0, 0, updateTime.width(), updateTime.height());
    p.end();
}

/* Draws a notification icon
 * x0,y0  top-left corner
 */
void drawIcon(int x0, int y0, int icon, char *screenBuffer)
{
    int i;

    for (i = 0; i <= LASTICON; i++)
        if (icon == iconsMap[i])
            break;
    if (i > LASTICON)
        return;

    QImage sb = QImage((unsigned char *)screenBuffer, 64, 128, QImage::Format_MonoLSB);
    QImage icons((unsigned char *)iconsBitmaps, iconsWidthPages * 8, iconsHeightPixels, QImage::Format_Mono);

    sb.setColorTable(QVector<QRgb>() << qRgb(0, 0, 0) << qRgb(255, 255, 255));
    icons.setColorTable(QVector<QRgb>() << qRgba(0, 0, 0, 0) << qRgb(255, 255, 255));

    QPainter p;
    p.begin(&sb);
    p.rotate(90);
    p.scale(1, -1);
    p.drawImage(x0, y0, icons, iconsStart[i], 0, iconsWidth[i], icons.height());
    p.end();


}

/* Draw small text to OLED
 * x0,y0  top-left corner
 *
 * Used for Battery percentage level and network type
 */
void drawSmallText(int x0, int y0, const char *text, char *screenBuffer)
{
    QImage sb = QImage((unsigned char *)screenBuffer, 64, 128, QImage::Format_MonoLSB);
    QImage pieniFontti((unsigned char *)pieniFonttiBitmaps, pieniFonttiWidthPages * 8, pieniFonttiHeightPixels, QImage::Format_Mono);

    sb.setColorTable(QVector<QRgb>() << qRgb(0, 0, 0) << qRgb(255, 255, 255));
    pieniFontti.setColorTable(QVector<QRgb>() << qRgba(0, 0, 0, 0) << qRgb(255, 255, 255));

    QPainter p;


    p.begin(&sb);
    p.rotate(90);
    p.scale(1, -1);


    for (; *text; text++) {
        int i;

        if (*text == ' ') {
            x0 += 4;
            continue;
        }

        for (i = 0; i < pieniFonttiNumOfChars; i++)
            if (pieniFonttiMap[i] == *text)
                break;

        if (i == pieniFonttiNumOfChars)
            continue;

        p.drawImage(x0, y0, pieniFontti, pieniFonttiStart[i], 0, pieniFonttiWidth[i], pieniFontti.height());
        x0 += pieniFonttiWidth[i];
    }

    p.end();
}

/* Draws digital clock to screen buffer */
void drawTime(int x0, int y0, const char *tNow, char *screenBuffer)
{
    QImage sb = QImage((unsigned char *)screenBuffer, 64, 128, QImage::Format_MonoLSB);
    QImage jollaFontti((unsigned char *)jollaFonttiBitmaps, jollaFonttiWidthPages * 8, jollaFonttiHeightPixels, QImage::Format_Mono);

    QPainter p;

    sb.setColorTable(QVector<QRgb>() << qRgb(0, 0, 0) << qRgb(255, 255, 255));
    jollaFontti.setColorTable(QVector<QRgb>() << qRgba(0, 0, 0, 0) << qRgb(255, 255, 255));

    p.begin(&sb);
    p.rotate(90);
    p.scale(1, -1);


    for (; *tNow; tNow++) {
        int i;

        if (*tNow == ' ') {
            x0 += 15;
            continue;
        }

        for (i = 0; i < 11; i++)
            if (jollaFonttiMap[i] == *tNow)
                break;

        if (i == 11)
            continue;

        p.drawImage(x0, y0, jollaFontti, jollaFonttiStart[i], 0, jollaFonttiWidth[i], jollaFontti.height());
        x0 += jollaFonttiWidth[i];
    }

    p.end();

}

/* Draws one pixel to x,y color 1=white, 0=black */
void drawPixel(int x, int y, int color, char *screenBuffer)
{
    QImage sb = QImage((unsigned char *)screenBuffer, 64, 128, QImage::Format_MonoLSB);
    sb.setPixel(y, x, !!color);

    /*
    char * sb = screenBuffer;

    if ((x < 0) || (x >= OLEDWIDTH) || (y < 0) || (y >= OLEDHEIGHT))
        return;

    // x is which column
    if (color != 0)
        (*(sb+(x*8)+(y/8))) |= 1 << ( y % 8 );
    else
        (*(sb+(x*8)+(y/8))) &= ~( 1 << ( y % 8 ));
	*/
}

/* Draws circle with radius r to x0,y0 color 1=white, 0=black */
void drawCircle(int x0, int y0, int r,  int color, char *screenBuffer)
{
    QImage sb = QImage((unsigned char *)screenBuffer, 64, 128, QImage::Format_MonoLSB);

    sb.setColorTable(QVector<QRgb>() << qRgb(0, 0, 0) << qRgb(255, 255, 255));

    QPainter p;

    p.begin(&sb);
    p.rotate(90);
    p.scale(1, -1);

    p.setPen(qRgb(color * 255, color * 255, color * 255));
    p.setBrush(Qt::NoBrush);
    p.drawEllipse(x0 - r, y0 - r, 2 * r, 2 * r);
    p.end();
}

/* Draws line from x0,y0 to x1,y1 color 1=white, 0=black */
void drawLine(int x0, int y0, int x1, int y1, int color, char *screenBuffer)
{
    QImage sb = QImage((unsigned char *)screenBuffer, 64, 128, QImage::Format_MonoLSB);

    sb.setColorTable(QVector<QRgb>() << qRgb(0, 0, 0) << qRgb(255, 255, 255));

    QPainter p;

    p.begin(&sb);
    p.rotate(90);
    p.scale(1, -1);

    p.setPen(qRgb(255 * color, 255 * color, 255 * color));
    p.drawLine(x0, y0, x1, y1);
    p.end();
}


#define clock_radius (32)
#define clock_x (64)
#define clock_y (32)

/* Draws analog clock facce to center of display */
void drawAnalogClock(int hours, int minutes, char *screenBuffer)
{
    char* sb = screenBuffer;
    int r;

    drawCircle(clock_x, clock_y, clock_radius, 1, sb);
    drawCircle(clock_x, clock_y, clock_radius-1, 1, sb);

    for (r=0; r < 12; r++)
    {
        float angle = (2 * pi / 12)*r;
        int tick_x_out = (clock_radius - 4) * cos (angle) + clock_x;
        int tick_y_out = (clock_radius - 4) * sin (angle) + clock_y;
        int tick_x_in = (clock_radius - 7) * cos (angle) + clock_x;
        int tick_y_in = (clock_radius - 7) * sin (angle) + clock_y;
        drawLine(tick_x_in, tick_y_in, tick_x_out, tick_y_out, 1, sb);
    }

    minuteHand.base_radius = 3;
    minuteHand.hand_radius = clock_radius-3;
    hourHand.base_radius = 3;
    hourHand.hand_radius = clock_radius-12;

    minuteHand.angle = ((2 * pi / 60) * minutes) - pi/2;
    hourHand.angle = ((2 * pi / 12) * (hours + ((float)minutes/60))) - pi/2;

    drawHand(minuteHand, 1, sb);
    drawHand(hourHand, 1, sb);

    drawCircle(clock_x, clock_y, 3, 1 ,sb);
    drawCircle(clock_x, clock_y, 2, 1 ,sb);
}

/* Draws a hand of analog clock */
void drawHand(const analogHand hand, int color, char *screenBuffer)
{
    char * sb = screenBuffer;
    int hand_base_x, hand_base_y;
    int hand_end_x, hand_end_y;

    hand_end_x = hand.hand_radius * cos (hand.angle) + clock_x;
    hand_end_y = hand.hand_radius * sin (hand.angle) + clock_y;

    for (int i = -hand.base_radius; i < hand.base_radius; i++)
    {
        if (i != 0)
        {
            hand_base_x = i * cos (hand.angle + pi / 2) + clock_x;
            hand_base_y = i * sin (hand.angle + pi / 2) + clock_y;
            drawLine(hand_base_x, hand_base_y, hand_end_x, hand_end_y, color, sb);
        }
        else
            drawLine(clock_x, clock_y, hand_end_x, hand_end_y, color, sb);
    }
}


/* Clears screen buffer */
void clearOled(char *screenBuffer)
{
    memset(screenBuffer, 0x00, SCREENBUFFERSIZE);
}


/* Draws screem buffer to OLED
 * Returns:
 *
 *  0  OK
 * -1  Open i2c failed
 * -2  ioctl failed
 * -3  write failed
 */
int updateOled(const char *screenBuffer)
{

    const char * sb = screenBuffer;
    int file, i;
    char buf[1025];

    buf[0] = 0x40;
    memcpy(&buf[1], sb, SCREENBUFFERSIZE);

    if ((file = open( "/dev/i2c-1", O_RDWR )) < 0)
    {
        return -1;
    }
    if (ioctl( file, I2C_SLAVE, 0x3c) < 0)
    {
        close(file);
        return -2;
    }

    if (write(file, buf, SCREENBUFFERSIZE+1) != SCREENBUFFERSIZE+1)
    {
        close(file);
        return -3;
    }

    close(file);

    return 0;
}

/* Contrast control
 * Returns:
 *
 *  0  OK
 * -1  Open i2c failed
 * -2  ioctl failed
 * -3  write failed
 */
int setContrastOled(unsigned int level)
{

    unsigned char contrast_seq[4] = { 0xd9, 0x7f, 0x81, 0x7f };
    unsigned char buf[2] = {0};
    int i, file;

    /* check that level is one of three valid ones */
    if ( (level != BRIGHTNESS_HIGH) && (level != BRIGHTNESS_LOW) && (level != BRIGHTNESS_MED))
        return -9;

    contrast_seq[1] = ((level >> 8 ) & 0xff);
    contrast_seq[3] = (level & 0xff);

    if ((file = open( "/dev/i2c-1", O_RDWR )) < 0)
    {
        return -1;
    }
    if (ioctl( file, I2C_SLAVE, 0x3c) < 0)
    {
        close(file);
        return -2;
    }

    buf[0] = 0x80;

    // send init sequence
    for (i=0; i<4; i++)
    {
        buf[1] = contrast_seq[i];
        if (write(file, buf, 2) != 2)
        {
            close(file);
            return -3;
        }
    }

    close(file);

    return 0;
}

/* Uses OLED screen internal inversion control to invert screen */
void invertOled(bool invert)
{
    unsigned char buf[2] = {0};
    int file;


    if ((file = open( "/dev/i2c-1", O_RDWR )) < 0)
    {
        return;
    }
    if (ioctl( file, I2C_SLAVE, 0x3c) < 0)
    {
        close(file);
        return;
    }

    buf[0] = 0x80;
    buf[1] = (invert ? 0xa7 : 0xa6);

    // send invert change sequence
    if (write(file, buf, 2) != 2)
    {
        close(file);
        return;
    }

    close(file);
}


/* Initializes OLED SSD1306 chip
 * Returns:
 *
 *  0  Initialisation ok
 * -1  Open i2c failed
 * -2  ioctl failed
 * -3  write failed
 */
int initOled(unsigned int level)
{
    unsigned char init_seq[30] = {0xae,         /* display off */
                                  0x20,0x01,    /* memory addressing mode = Vertical 01 Horizontal 00 */
                                  0xb0,         /* page start address */
                                  0xc0,         /* scan direction */
                                  0x00,         /* lower column start address */
                                  0x10,         /* higher column start address */
                                  0x40,         /* display start line */
                                  0x81,0xcf,    /* contrast */
                                  0xa0,         /* segment remap */
                                  0xa6,         /* normal display  (a7 = inverse) */
                                  0xa8,0x3f,    /* mux ratio = 64 */
                                  0xa4,         /* display follows gdram */
                                  0xd3,0x00,    /* display offset = 0*/
                                  0xd5,0xf0,    /* oscillator */
                                  0xd9,0xf1,    /* precharge period */
                                  0xda,0x12,    /* configure COM pins */
                                  0xdb,0x40,    /* set VCOM level */
                                  0x23,0x00,    /* disable blinks and fading */
                                  0x8d,0x14,    /* enable charge pump. (0x10 disables) */
                                  0xaf};        /* display on*/
								  
    int i, file;
    unsigned char buf[2] = {0};

    /* Override contrast/precharge level if valid one given */
    if ( (level == BRIGHTNESS_HIGH) || (level == BRIGHTNESS_LOW) || (level == BRIGHTNESS_MED))
    {
        init_seq[20] = ((level >> 8 ) & 0xff);
        init_seq[9] = (level & 0xff);
    }

    usleep(200000); /* Wait for 200 ms */

    if ((file = open( "/dev/i2c-1", O_RDWR )) < 0)
    {
        return -1;
    }
    if (ioctl( file, I2C_SLAVE, 0x3c) < 0)
    {
        close(file);
        return -2;
    }

    buf[0] = 0x80; // control reg

    // send init sequence
    for (i=0; i<30; i++)
    {
        buf[1] = init_seq[i];
        if (write(file, buf, 2) != 2)
        {
            close(file);
            return -3;
        }
    }

	close(file);

    usleep(100000); /* wait 100 ms */
	
	return 0;
}

/* Shutdown OLED - Must be done before disabling VDD
 * Returns:
 *
 *  0  Deinitialisation ok
 * -1  Open i2c failed
 * -2  ioctl failed
 * -3  write failed
 */
int deinitOled()
{
    unsigned char init_seq[3] = {0xae, /* display off */
                                 0x8d,0x14}; /* disable charge pump. (0x14 enables) */


    int i, file;
    unsigned char buf[2] = {0};

    if ((file = open( "/dev/i2c-1", O_RDWR )) < 0)
    {
        return -1;
    }
    if (ioctl( file, I2C_SLAVE, 0x3c) < 0)
    {
        close(file);
        return -2;
    }

    buf[0] = 0x80; // control reg

    // send init sequence
    for (i=0; i<3; i++)
    {
        buf[1] = init_seq[i];
        if (write(file, buf, 2) != 2)
        {
            close(file);
            return -3;
        }
    }

    close(file);

    usleep(100000); /* wait 100 ms */

    return 0;

}

